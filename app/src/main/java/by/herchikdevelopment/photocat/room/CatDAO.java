package by.herchikdevelopment.photocat.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import by.herchikdevelopment.photocat.entity.Cat;
import io.reactivex.Flowable;

@Dao
public interface CatDAO {
    @Insert
    void insertFavoriteCat(Cat cat);

    @Query("Select * from cat")
    Flowable<List<Cat>> getFavoriteCats();

    @Delete
    void deleteCat(Cat cat);
}
