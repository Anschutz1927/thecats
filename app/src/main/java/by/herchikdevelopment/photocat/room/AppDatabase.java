package by.herchikdevelopment.photocat.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import by.herchikdevelopment.photocat.entity.Cat;

@Database(entities = Cat.class, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public abstract CatDAO dao();

    public static synchronized AppDatabase getInstance() {
        return instance;
    }

    public static void initDatabase(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, AppDatabase.class, "cats_database")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
    }
}
