package by.herchikdevelopment.photocat.mvp.views;

import java.util.List;

import by.herchikdevelopment.photocat.entity.Cat;

public interface IFavoriteCatsView extends BaseCatView {

    void deletionCompleted();

    void setCats(List<Cat> cats);
}
