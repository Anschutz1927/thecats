package by.herchikdevelopment.photocat.mvp.models;

import java.util.ArrayList;
import java.util.List;

import androidx.paging.PagedList;
import by.herchikdevelopment.photocat.entity.Cat;
import by.herchikdevelopment.photocat.network.Network;
import by.herchikdevelopment.photocat.room.AppDatabase;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CatModel {

    private final AppDatabase database = AppDatabase.getInstance();
    private final Network network = Network.getInstance();
    private List<Cat> favoriteCats = new ArrayList<>();

    private CatModel() {}

    public List<Cat> getFavoriteCats() {
        return favoriteCats;
    }

    public Disposable insertFavoriteCat(Cat cat, Action onComplete, Consumer<Throwable> onError) {
        return Completable.fromAction(() -> database.dao().insertFavoriteCat(cat))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(onComplete, onError);
    }

    public Disposable deleteCat(Cat cat, Action onComplete, Consumer<Throwable> onError) {
        return Completable.fromAction(() -> database.dao().deleteCat(cat))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(onComplete, onError);
    }

    public Disposable requestFavoriteCats(Action onCatsUpdated) {
        return database.dao().getFavoriteCats()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(cats -> {
                    favoriteCats = cats;
                    onCatsUpdated.run();
                });
    }

    public Disposable loadPagedList(Consumer<PagedList<Cat>> onResult, Consumer<Throwable> onError) {
        return network.pagedLoadingCats()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(onResult, onError, () -> System.out.println("onComplete"));
    }

    public static class Builder {

        public CatModel build() {
            return new CatModel();
        }
    }
}
