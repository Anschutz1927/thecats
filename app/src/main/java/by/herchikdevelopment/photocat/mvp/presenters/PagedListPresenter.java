package by.herchikdevelopment.photocat.mvp.presenters;

import javax.inject.Inject;

import by.herchikdevelopment.photocat.dagger.AppDagger;
import by.herchikdevelopment.photocat.entity.Cat;
import by.herchikdevelopment.photocat.mvp.models.CatModel;
import by.herchikdevelopment.photocat.mvp.views.IFavoriteCatsView;
import by.herchikdevelopment.photocat.mvp.views.IPagedListView;

public class PagedListPresenter extends BaseCatPresenter {

    private final IPagedListView view;
    @Inject
    CatModel model;

    private PagedListPresenter(IPagedListView view) {
        this.view = view;
    }

    @Override
    public void downloadPicture(Cat cat) {
        view.onDownloadPicture(cat.getUrl());
    }

    public void insertFavoriteCat(Cat cat) {
        addDisposable(model.insertFavoriteCat(cat, view::onInsertCatReady, Throwable::printStackTrace));
    }

    public void subscribeOnPagedList() {
        addDisposable(model.loadPagedList(view::onPagedListReady, Throwable::printStackTrace));
    }

    public static class Builder {

        private final IPagedListView view;

        public Builder(IPagedListView view) {
            this.view = view;
        }

        public PagedListPresenter build() {
            PagedListPresenter obj = new PagedListPresenter(view);
            AppDagger.INSTANCE.inject(obj);
            return obj;
        }
    }
}
