package by.herchikdevelopment.photocat.mvp.presenters;

import javax.inject.Inject;

import by.herchikdevelopment.photocat.dagger.AppDagger;
import by.herchikdevelopment.photocat.entity.Cat;
import by.herchikdevelopment.photocat.mvp.models.CatModel;
import by.herchikdevelopment.photocat.mvp.views.IFavoriteCatsView;

public class FavoriteCatsPresenter extends BaseCatPresenter {

    private final IFavoriteCatsView view;
    @Inject
    CatModel model;

    private FavoriteCatsPresenter(IFavoriteCatsView view) {
        this.view = view;
    }

    @Override
    public void downloadPicture(Cat cat) {
        view.onDownloadPicture(cat.getUrl());
    }

    public void deleteCat(Cat cat) {
        addDisposable(model.deleteCat(cat, view::deletionCompleted, Throwable::printStackTrace));
    }

    public void requestFavoriteCats() {
        addDisposable(model.requestFavoriteCats(() -> view.setCats(model.getFavoriteCats())));
    }

    public static class Builder {

        private final IFavoriteCatsView view;

        public Builder(IFavoriteCatsView view) {
            this.view = view;
        }

        public FavoriteCatsPresenter build() {
            FavoriteCatsPresenter obj = new FavoriteCatsPresenter(view);
            AppDagger.INSTANCE.inject(obj);
            return obj;
        }
    }
}
