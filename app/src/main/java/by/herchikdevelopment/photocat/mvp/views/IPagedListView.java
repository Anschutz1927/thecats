package by.herchikdevelopment.photocat.mvp.views;

import androidx.paging.PagedList;

import by.herchikdevelopment.photocat.entity.Cat;

public interface IPagedListView extends BaseCatView {

    void onPagedListReady(PagedList<Cat> cats);

    void onInsertCatReady();
}
