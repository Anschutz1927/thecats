package by.herchikdevelopment.photocat.mvp.presenters;

import by.herchikdevelopment.photocat.entity.Cat;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BaseCatPresenter {

    private final CompositeDisposable disposables = new CompositeDisposable();

    protected void addDisposable(Disposable... disposables) {
        this.disposables.addAll(disposables);
    }

    protected void dispose() {
        synchronized (disposables) {
            disposables.dispose();
        }
    }

    public void onDestroyView() {
        dispose();
    }

    public abstract void downloadPicture(Cat cat);
}
