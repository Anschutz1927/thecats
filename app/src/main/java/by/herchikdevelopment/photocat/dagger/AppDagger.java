package by.herchikdevelopment.photocat.dagger;

import androidx.fragment.app.Fragment;

import javax.inject.Singleton;

import by.herchikdevelopment.photocat.mvp.models.CatModel;
import by.herchikdevelopment.photocat.mvp.presenters.FavoriteCatsPresenter;
import by.herchikdevelopment.photocat.mvp.presenters.PagedListPresenter;
import by.herchikdevelopment.photocat.mvp.views.IFavoriteCatsView;
import by.herchikdevelopment.photocat.mvp.views.IPagedListView;
import by.herchikdevelopment.photocat.ui.adapters.ViewPagerAdapter;
import by.herchikdevelopment.photocat.ui.fragments.FavoriteCatsFragment;
import by.herchikdevelopment.photocat.ui.fragments.PagedListCatsFragment;
import dagger.Component;
import dagger.Module;
import dagger.Provides;

public class AppDagger {

    public static AppComponent INSTANCE;

    public static void initDagger() {
        INSTANCE = DaggerAppDagger_AppComponent.builder().build();
    }

    @Singleton
    @Component(modules = {ModelModule.class, FavouritesCatsModule.class, PagedListCatsModule.class})
    public interface AppComponent {

        void inject(ViewPagerAdapter viewPagerAdapter);

        void inject(FavoriteCatsFragment fragment);

        void inject(PagedListCatsFragment fragment);

        void inject(FavoriteCatsPresenter presenter);

        void inject(PagedListPresenter presenter);

    }

    @Module
    public static class ModelModule {

        @Singleton
        @Provides
        public CatModel provideCatModel() {
            return new CatModel.Builder().build();
        }
    }

    @Module
    public static class FavouritesCatsModule {

        private final Fragment fragment = new FavoriteCatsFragment();

        @Provides
        public FavoriteCatsFragment provideFavouriteFragment() {
            return (FavoriteCatsFragment) fragment;
        }

        @Provides
        public IFavoriteCatsView provideFavouriteCatsView() {
            return (IFavoriteCatsView) this.fragment;
        }

        @Provides
        public FavoriteCatsPresenter provideFavouritesCatsPresenter(IFavoriteCatsView view) {
            return new FavoriteCatsPresenter.Builder(view).build();
        }

    }

    @Module
    public static class PagedListCatsModule {

        private final Fragment fragment = new PagedListCatsFragment();

        @Provides
        public PagedListCatsFragment providePagedListCatsFragment() {
            return (PagedListCatsFragment) fragment;
        }

        @Provides
        public IPagedListView providePagedListView() {
            return (IPagedListView) this.fragment;
        }

        @Provides
        public PagedListPresenter provideFavouritesCatsPresenter(IPagedListView view) {
            return new PagedListPresenter.Builder(view).build();
        }
    }
}
