package by.herchikdevelopment.photocat.network;

import java.util.List;

import by.herchikdevelopment.photocat.entity.Cat;
import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface CatApiService {
    @Headers("api_key: 887a4bcc-0d1f-49de-ae60-0b7374af675c")
    @GET("images/search")
    Flowable<List<Cat>> getCats(@Query("page") int page, @Query("limit") int limit);
}
