package by.herchikdevelopment.photocat.network.pagedList;

import androidx.annotation.NonNull;
import androidx.paging.DataSource;
import androidx.paging.PositionalDataSource;

import by.herchikdevelopment.photocat.entity.Cat;
import by.herchikdevelopment.photocat.network.CatApiService;

public class CatDataSource extends PositionalDataSource<Cat> {

    private final CatApiService api;

    public CatDataSource(CatApiService api) {
        this.api = api;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams params, @NonNull LoadInitialCallback<Cat> callback) {
        api.getCats(params.requestedStartPosition, params.requestedLoadSize)
                .blockingSubscribe(cats -> callback.onResult(cats, 0), Throwable::printStackTrace);
    }

    @Override
    public void loadRange(@NonNull LoadRangeParams params, @NonNull LoadRangeCallback<Cat> callback) {
        api.getCats(params.startPosition, params.loadSize)
                .blockingSubscribe(callback::onResult, Throwable::printStackTrace);
    }

    public static class Factory extends DataSource.Factory<Integer, Cat> {

        private final CatApiService api;

        public Factory(CatApiService api) {
            this.api = api;
        }

        @NonNull
        @Override
        public DataSource<Integer, Cat> create() {
            return new CatDataSource(api);
        }
    }
}
