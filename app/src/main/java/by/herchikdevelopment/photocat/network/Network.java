package by.herchikdevelopment.photocat.network;

import androidx.paging.PagedList;
import androidx.paging.RxPagedListBuilder;

import java.util.concurrent.TimeUnit;

import by.herchikdevelopment.photocat.entity.Cat;
import by.herchikdevelopment.photocat.network.pagedList.CatDataSource;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Network {

    private static Network instance;
    private final static String BASE_URL = "https://api.thecatapi.com/v1/";
    private final CatApiService catApi;

    public static Network getInstance() {
        if (instance == null) {
            instance = new Network();
        }
        return instance;
    }

    private Network() {
        final OkHttpClient httpClient = new OkHttpClient.Builder()
                .callTimeout(45, TimeUnit.SECONDS)
                .connectTimeout(45, TimeUnit.SECONDS)
                .readTimeout(45, TimeUnit.SECONDS)
                .writeTimeout(45, TimeUnit.SECONDS)
                .build();
        catApi = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .client(httpClient)
                .build()
                .create(CatApiService.class);
    }

    public Flowable<PagedList<Cat>> pagedLoadingCats() {
        return new RxPagedListBuilder<>(
                new CatDataSource.Factory(catApi),
                new PagedList.Config.Builder().setEnablePlaceholders(false).setInitialLoadSizeHint(30).setPageSize(10).build()
        ).buildFlowable(BackpressureStrategy.LATEST);
    }
}
