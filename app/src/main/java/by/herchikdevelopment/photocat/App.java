package by.herchikdevelopment.photocat;

import android.app.Application;

import by.herchikdevelopment.photocat.dagger.AppDagger;
import by.herchikdevelopment.photocat.room.AppDatabase;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppDatabase.initDatabase(this);
        AppDagger.initDagger();
    }
}
