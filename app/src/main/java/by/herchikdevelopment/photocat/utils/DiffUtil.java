package by.herchikdevelopment.photocat.utils;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import by.herchikdevelopment.photocat.entity.Cat;

public class DiffUtil extends androidx.recyclerview.widget.DiffUtil.ItemCallback<Cat> {

    @Override
    public boolean areItemsTheSame(@NonNull Cat oldItem, @NonNull Cat newItem) {
        return oldItem == newItem;
    }

    @SuppressLint("DiffUtilEquals")
    @Override
    public boolean areContentsTheSame(@NonNull Cat oldItem, @NonNull Cat newItem) {
        return oldItem.getId().equals(newItem.getId()) && oldItem.getUrl().equals(newItem.getUrl());
    }
}
