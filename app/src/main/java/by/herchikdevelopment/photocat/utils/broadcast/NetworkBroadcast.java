package by.herchikdevelopment.photocat.utils.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import by.herchikdevelopment.photocat.utils.NetworkUtils;

public class NetworkBroadcast extends BroadcastReceiver {
    private final OnNetworkConnection onConnection;

    public NetworkBroadcast(OnNetworkConnection onConnection) {
        this.onConnection = onConnection;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!NetworkUtils.isNetworkConnected(context)) {
            Toast.makeText(context, "Check the network connection", Toast.LENGTH_LONG).show();
        } else {
            onConnection.onNetworkConnection();
        }
    }

    public interface OnNetworkConnection {
        void onNetworkConnection();
    }
}
