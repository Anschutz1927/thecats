package by.herchikdevelopment.photocat.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import by.herchikdevelopment.photocat.R;
import by.herchikdevelopment.photocat.entity.Cat;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.CatHolder> {

    private final OnItemClickListener onItemClickListener;
    private List<Cat> cats = new ArrayList<>();

    public RecyclerViewAdapter(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public CatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CatHolder holder = new CatHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_view, parent, false));
        holder.menuImageView.setOnClickListener(view ->
                onItemClickListener.onClickPopupMenu(view, cats.get(holder.getAdapterPosition())));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CatHolder holder, int position) {
        holder.nameTextView.setText(cats.get(position).getId());
        Glide.with(holder.itemView.getContext())
                .load(cats.get(position).getUrl())
                .placeholder(R.drawable.ic_cat)
                .circleCrop()
                .into(holder.photoCatImageView);
    }

    @Override
    public int getItemCount() {
        return cats.size();
    }

    public void setListToAdapter(List<Cat> cats) {
        this.cats = cats;
        notifyDataSetChanged();
    }

    static class CatHolder extends RecyclerView.ViewHolder {

        private final TextView nameTextView;
        private final ImageView photoCatImageView;
        private final ImageView menuImageView;

        private CatHolder(@NonNull View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.nameTextView);
            photoCatImageView = itemView.findViewById(R.id.photoCatImageView);
            menuImageView = itemView.findViewById(R.id.menuImageView);
        }
    }
}
