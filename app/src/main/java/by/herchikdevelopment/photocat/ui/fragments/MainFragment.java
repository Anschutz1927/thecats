package by.herchikdevelopment.photocat.ui.fragments;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.view.View;

import androidx.fragment.app.Fragment;

import by.herchikdevelopment.photocat.entity.Cat;
import by.herchikdevelopment.photocat.ui.adapters.OnItemClickListener;

public abstract class MainFragment extends Fragment implements OnItemClickListener {


    public abstract void popupMenu(View view, Cat cat);

    public void downloadPicture(String stringUrl) {
        Uri uri = Uri.parse(stringUrl);
        DownloadManager.Request request = new DownloadManager.Request(uri)
                .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE |
                        DownloadManager.Request.NETWORK_WIFI)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setTitle("Downloading a file")
                .setDestinationInExternalPublicDir(
                        Environment.DIRECTORY_PICTURES, uri.getLastPathSegment());
        ((DownloadManager) requireContext().getSystemService(Context.DOWNLOAD_SERVICE))
                .enqueue(request);
    }
}
