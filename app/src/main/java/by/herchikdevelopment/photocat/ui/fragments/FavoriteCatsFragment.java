package by.herchikdevelopment.photocat.ui.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import by.herchikdevelopment.photocat.R;
import by.herchikdevelopment.photocat.dagger.AppDagger;
import by.herchikdevelopment.photocat.entity.Cat;
import by.herchikdevelopment.photocat.mvp.presenters.FavoriteCatsPresenter;
import by.herchikdevelopment.photocat.mvp.views.IFavoriteCatsView;
import by.herchikdevelopment.photocat.ui.adapters.RecyclerViewAdapter;

public class FavoriteCatsFragment extends MainFragment implements IFavoriteCatsView {

    @Inject
    FavoriteCatsPresenter presenter;
    private RecyclerViewAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AppDagger.INSTANCE.inject(this);
        View view = inflater.inflate(R.layout.fragment_favorite_cats, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        adapter = new RecyclerViewAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        presenter.requestFavoriteCats();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onDestroyView();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void popupMenu(View view, Cat cat) {
        final PopupMenu popupMenu = new PopupMenu(requireContext(), view);
        popupMenu.inflate(R.menu.popup_menu_favotire_cat);
        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.itemDeleteCat) {
                presenter.deleteCat(cat);
            } else if (item.getItemId() == R.id.itemDownloadPicture) {
                presenter.downloadPicture(cat);
            }
            return true;
        });
        popupMenu.show();
    }

    @Override
    public void onClickPopupMenu(View view, Cat cat) {
        popupMenu(view, cat);
    }

    @Override
    public void setCats(List<Cat> cats) {
        adapter.setListToAdapter(cats);
    }


    @Override
    public void onDownloadPicture(String stringUri) {
        downloadPicture(stringUri);
    }

    @Override
    public void deletionCompleted() {
        Toast.makeText(getContext(), "Favorite cat has been deleted", Toast.LENGTH_SHORT).show();
    }
}
