package by.herchikdevelopment.photocat.ui.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import javax.inject.Inject;

import by.herchikdevelopment.photocat.dagger.AppDagger;
import by.herchikdevelopment.photocat.ui.fragments.FavoriteCatsFragment;
import by.herchikdevelopment.photocat.ui.fragments.PagedListCatsFragment;

public class ViewPagerAdapter extends FragmentStateAdapter {

    private final int COUNT_PAGES = 2;
    @Inject
    PagedListCatsFragment pagedListCatsFragment;
    @Inject
    FavoriteCatsFragment favoriteCatsFragment;

    public ViewPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
        AppDagger.INSTANCE.inject(this);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return pagedListCatsFragment;
            case 1:
                return favoriteCatsFragment;
        }
        return pagedListCatsFragment;
    }

    @Override
    public int getItemCount() {
        return COUNT_PAGES;
    }
}
