package by.herchikdevelopment.photocat.ui.fragments;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import javax.inject.Inject;

import by.herchikdevelopment.photocat.R;
import by.herchikdevelopment.photocat.dagger.AppDagger;
import by.herchikdevelopment.photocat.entity.Cat;
import by.herchikdevelopment.photocat.mvp.presenters.PagedListPresenter;
import by.herchikdevelopment.photocat.mvp.views.IPagedListView;
import by.herchikdevelopment.photocat.ui.adapters.CatsPagedListAdapter;
import by.herchikdevelopment.photocat.utils.DiffUtil;
import by.herchikdevelopment.photocat.utils.broadcast.NetworkBroadcast;

public class PagedListCatsFragment extends MainFragment
        implements IPagedListView, NetworkBroadcast.OnNetworkConnection {

    private final CatsPagedListAdapter adapter = new CatsPagedListAdapter(new DiffUtil(), this);
    private final NetworkBroadcast networkBroadcast = new NetworkBroadcast(this);
    @Inject
    PagedListPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AppDagger.INSTANCE.inject(this);
        View view = inflater.inflate(R.layout.fragment_cats, container, false);
        final RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        registerNetworkBroadcast();
        presenter.subscribeOnPagedList();
        return view;
    }

    @Override
    public void onDestroyView() {
        unregisterNetworkBroadcast();
        presenter.onDestroyView();
        super.onDestroyView();
    }

    @Override
    public void popupMenu(View view, Cat cat) {
        final PopupMenu popupMenu = new PopupMenu(requireContext(), view);
        popupMenu.inflate(R.menu.popup_menu);
        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.itemAddFavoriteCat) {
                presenter.insertFavoriteCat(cat);
            } else if (item.getItemId() == R.id.itemDownloadPicture) {
                presenter.downloadPicture(cat);
            }
            return true;
        });
        popupMenu.show();
    }

    @Override
    public void onClickPopupMenu(View view, Cat cat) {
        popupMenu(view, cat);
    }

    @Override
    public void onPagedListReady(PagedList<Cat> cats) {
        adapter.submitList(cats);
    }

    @Override
    public void onInsertCatReady() {
        Toast.makeText(getContext(), "The cat has been inserted to favorites", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDownloadPicture(String stringUri) {
        downloadPicture(stringUri);
    }

    @Override
    public void onNetworkConnection() {
        if (adapter.getCurrentList() != null) {
            adapter.getCurrentList().getDataSource().invalidate();
        }
    }

    private void registerNetworkBroadcast() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        requireContext().registerReceiver(networkBroadcast, filter);
    }

    private void unregisterNetworkBroadcast() {
        requireContext().unregisterReceiver(networkBroadcast);
    }
}
