package by.herchikdevelopment.photocat.ui.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import by.herchikdevelopment.photocat.R;
import by.herchikdevelopment.photocat.ui.adapters.ViewPagerAdapter;

public class PhotoCatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_cat);
        initViewPager();
    }

    private void initViewPager() {
        ViewPager2 viewPager = findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        viewPager.setAdapter(new ViewPagerAdapter(this));
        new TabLayoutMediator(tabLayout, viewPager,
                (tab, position) -> {
                    switch (position) {
                        case 0:
                            tab.setText("Cats"); //TODO need to be moved to resources
                            break;
                        case 1:
                            tab.setText("Favorite Cats"); //TODO need to be moved to resources
                            break;
                    }
                }
        ).attach();
    }
}
