package by.herchikdevelopment.photocat.ui.adapters;

import android.view.View;

import by.herchikdevelopment.photocat.entity.Cat;

public interface OnItemClickListener {
    void onClickPopupMenu(View view, Cat cat);
}
